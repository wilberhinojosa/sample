# Shared Library: 
## Components: 
  The following elements we can isolate and keep a shared library across all reporting applications:
### The following form inputs are required to have things working and all three applications share them:
#### Form Inputs
We will use react-final-form to handle:
  * Text
  * Radio
  * Checkbox
  * TimePicker
  * Text & HTML Editor
  * List
  * Nested Lists (Podcast Category)
  * FileUpload:
    - AudioFileUploader
    - ImageFileUploader
  * ToogleButton
  * Form groups: Section with hiding options: The episode editing page shows sections with a link at the top of its title which displays more fields available to the user

#### Side panel widgets:
  * File Uploader. Same (Image uploader) in a different view
  * Summaries : List of data displayed in tables / data list
#### HOC's:
  We should enforce to use and compose HOC's
  * ClickToEdit
  * asFinalField 

#### Utilities
  * validators

#### Tables and pagination

## Architecture and pattern design
We are going to keep all our components without any dependency from external context. They will be defined only by their properties.
The external modules will be imported as simple as possible. Avoid importing all module at once.
we should follow the a functional pattern to import/export and use common components.
All HOC's should be written in a functional pattern in a way to allow composition and enforce to use *recompose*.
We will define write some renderers as declarative components that will render different elements in target elements. such as: Table cells in tables or Input fields in forms.
Tables and forms will be rendered according to their input definition list or a column definition list.
Example of input definition
```
const inputDefinitions = [
  {
    name: 'field_1',
    label: 'Field 1',
    renderer: InputRenderer,
  },
  {
    name: 'name_of_the_field'
    label: 'Label To Display',
    renderer: compose(
      onFeatureFlag('SOME_FEATURE'),
      onValuesCheck(values => values['some-select'] === 'episodic')
    )(InputRenderer),
  }
]
```
An example of the renderer and composite object is: [Example](https://bitbucket.org/wilberhinojosa/sample/src/790a4a4f7af43053c6e8774e37b81f2bec87c0e2/src/App.js#lines-40)
## Styling
The shared library will use the default bootstrap styling and inside the specific applications we are going to : edit and customize stylesheet bootstrap files to avoid overwritting styles:
Two options to follow:
  - Importing and customizing directly 
  - Or create a themes repository

## Best Practices
  * Enforce to write unit tests
  * Enforce to extend all React components from React.PureComponent or create functional ones
  * Do not use getDerivedStateFromProps or componentWillReceiveProps as few as possible
  * Syntax checker and follow commonly accepted rules
  * Use of ES6 specification or later
  * Use of lodash utility library. Instead of `myObject && myObject.nesterProperty && myObject.nesterProperty.simpleValue`
    we should enforce to use lodash/get : 
    ```
    const simpleValue = _get(myObject, 'nesterProperty.simpleValue')
    ```

#### Optional tools
  * react-bootstrap
  * react-intl
