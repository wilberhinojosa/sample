import Input from './Input'
import Dropdown from './Dropdown'
import Radio from './Radio'

import withFinalField from './withFinalField'

export const InputRenderer = withFinalField()(Input)
export const DropdownRenderer = withFinalField()(Dropdown)
export const RadioRenderer = withFinalField()(Radio)
