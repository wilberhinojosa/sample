import React from 'react'
import { Dropdown, Row, Col, Form } from 'react-bootstrap'
import { Field } from 'react-final-form'
import _get from 'lodash/get'
import withFinalField from './withFinalField'

class DropdownRenderer extends React.PureComponent {
  handleSelect = value => {
    this.props.onChange(value)
  }
  render() {
    const { options, value, label: fieldLabel} = this.props
    const fieldValue = options.find(opt => opt.value === value)
    return (
      <Form.Group as={Row}>
        <Form.Label column sm="2">{ fieldLabel }</Form.Label>
        <Col sm="10">
          <Dropdown>
            <Dropdown.Toggle variant="success" id="dropdown-basic">
              { fieldValue ? fieldValue.label: 'Select one' }
            </Dropdown.Toggle>
            <Dropdown.Menu>
              { options.map(option => {
                const { value, label } = option
                return <Dropdown.Item 
                  key={value}
                  onSelect={() => this.handleSelect(value)}
                  active={fieldValue && value === fieldValue.value}
                >
                  { label }
                </Dropdown.Item>
              })
              }
            </Dropdown.Menu>
          </Dropdown>
        </Col>
      </Form.Group>
    )
  }
}

export default withFinalField()(DropdownRenderer)
