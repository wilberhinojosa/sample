import React from 'react'
import { Row, Col, Form } from 'react-bootstrap'
import { Field } from 'react-final-form'
import _get from 'lodash/get'
import withFinalField from './withFinalField'


class RadioRenderer extends React.PureComponent {
  render() {
    const { input, options, label: fieldLabel, name: fieldName, onChange} = this.props
    return (
      <Form.Group as={Row}>
        <Form.Label column sm="2">{ fieldLabel }</Form.Label>
        <Col sm="10">
          { options.map(option => {
            const { value, label } = option
            return (<Form.Check
              inline
              custom
              label={label}
              key={value}
              value={value}
              name={fieldName}
              type="radio"
              onChange={onChange}
            />)
          })
          }
        </Col>
      </Form.Group>
    )
  }
}

export default withFinalField()(RadioRenderer)
