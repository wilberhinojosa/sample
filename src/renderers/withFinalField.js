import React from 'react'
import { Field } from 'react-final-form'

function withFinalField() {
  return Component => props => {
    const { name, type } = props
    return <Field
      name={name}
      type={type}
    >
      {
        (fieldProps) => <Component {...props}
          {...fieldProps} 
          onChange={fieldProps.input.onChange}
          value={fieldProps.input.value}
          />
      }
    </Field>
  }
}

export default withFinalField
