import React from 'react'
import PropTypes from 'prop-types'
import { Col, Row, Form } from 'react-bootstrap'
import { Field } from 'react-final-form'
import withFinalField from './withFinalField'

export class InputRenderer extends React.PureComponent {
  render() {
    const { id, name, label, input, labelCols } = this.props
    return (
      <Form.Group as={Row}>
        <Form.Label htmlFor={id} column sm={labelCols}>{ label }</Form.Label>
        <Col sm="10">
          <Form.Control id={id} name="name" {...input} />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Col>
      </Form.Group>
    )
  }
}

InputRenderer.propTypes = {
  labelCols: PropTypes.number,
  fieldCols: PropTypes.number,
}

InputRenderer.defaultProps = {
  labelCols: 2,
  fieldCols: 10,
}

export default withFinalField()(InputRenderer)
