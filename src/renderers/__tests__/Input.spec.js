import React from 'react'
import { mount } from 'enzyme'
import InputRendererContainer, {InputRenderer}  from '../Input'
import { Form } from 'react-final-form'

describe('some', () => {
  it('should render', () => {
    const wrapper = mount(<InputRenderer name="title" label="Super Label" />)
    console.log(wrapper.debug());
  })

  it('should render with container', () => {
    const wrapper = mount(
      <Form
        onSubmit={jest.fn()}
        render={ () => (
          <InputRenderer name="title" label="Super Label" />
        )}
      />
    )
    console.log(wrapper.debug());
  })
})
