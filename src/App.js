import React from 'react';
import './App.css';
import { Form as FinalForm, Field } from 'react-final-form'
import compose from 'recompose/compose'
import { Form, Container, Row, Col, Button } from 'react-bootstrap'
import {
  InputRenderer,
  RadioRenderer,
  DropdownRenderer,
} from './renderers'
import 'bootstrap/dist/css/bootstrap.css'

function onValuesCheck(fn) {
  return Component => props => {
    if (fn(props.values)) {
      return null
    }
    return <Component {...props} />
  }
}

function ifFeatureEnabled(flag) {
  return true
}

function onFeatureFlag(flag) {
  return Component => props => {
    if (ifFeatureEnabled(flag)) {
      return <Component {...props} />
    }
    return null
  }
}

const fields = [
  {
    id: 'form-title',
    name: 'title',
    label: 'Title',
    renderer: compose(
      onFeatureFlag('SOME_FEATURE'),
      onValuesCheck(values => values['some-select'] === 'episodic')
    )(InputRenderer),
    type: 'text',
  },
  {
    id: 'form-description',
    name: 'description',
    label: 'Description',
    renderer: InputRenderer,
    type: 'text',
  },
  {
    id: 'form-itunes-show-ty',
    name: 'itunes-show-type',
    label: 'iTunes show type',
    type: 'radio',
    renderer: RadioRenderer,
    options: [
      {
        label: 'Episodic',
        value: 'episodic'
      },
      {
        label: 'Serial',
        value: 'serial'
      }
    ],
  },
  {
    name: 'some-select',
    label: 'Select me',
    renderer: DropdownRenderer,
    type: 'select',
    options: [
      {
        label: 'Episodic',
        value: 'episodic'
      },
      {
        label: 'Serial',
        value: 'serial'
      }
    ]
  },
  {
    name: 'language',
    label: 'Select Language',
    renderer: DropdownRenderer,
    type: 'select',
    options: [
      {
        label: 'English',
        value: 'en'
      },
      {
        label: 'Spanish',
        value: 'sp'
      }
    ]
  },
  {
    name: 'categories',
    label: 'Website Categories',
    renderer: () => null,
  }
]

const values = {
  'some-select': 'episodic',
  title: 'The title of the problem',
  description: 'The description fo the problem',
  'itunes-show-type': 'serial',
}


class App extends React.PureComponent {
  renderFields(values) {
    return fields.map(field => {
      const { id, type, options, name, label, renderer: Renderer} = field
      return <Renderer
        id={id}
        key={name}
        type={type}
        field={field}
        name={name}
        label={label}
        options={options}
        values={values}
        />
    })
  }

  handleSubmit = data => {
    console.log('this should not be: %o', data);
  }
  render() {
    return (
      <Container>
        <Row>
          <Col>
            <FinalForm
              onSubmit={this.handleSubmit}
              initialValues={values}
              render={({ handleSubmit, form, submitting, pristine, values }) => (
                <Form onSubmit={handleSubmit}>
                  { this.renderFields(values) }
                  <Button type="submit">Send </Button>
                </Form>
              )}
            />
          </Col>
        </Row>
      </Container>
    )
  }
}

export default App;
